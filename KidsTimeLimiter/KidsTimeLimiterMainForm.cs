﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Xml.Serialization;
using System.IO;
using System.Configuration;

using log4net;

using WindowsFormsCalendar;

using KidsTimeLimiter.Configuration;

// TODO: Force messages boxes to front
// TODO: Detect clock changes

namespace KidsTimeLimiter
{
    public partial class Form1 : Form
    {
        #region Fields

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private List<CalendarItem> m_ScheduleItems = new List<CalendarItem>();

        Thread TimeMonitorThread = null;
        TimeMonitor TimeMonitor = null;

        #endregion

        // http://www.c-sharpcorner.com/UploadFile/f9f215/how-to-minimize-your-application-to-system-tray-in-C-Sharp/
        // https://msdn.microsoft.com/en-us/library/system.configuration.configurationmanager.appsettings(v=vs.110).aspx
        public Form1()
        {
            InitializeComponent();

            // Load Settings
            string test1 = Properties.Settings.Default.DefaultScheduleFile;
            Properties.Settings.Default.DefaultScheduleFile = "some new value";
            Properties.Settings.Default.Save();
            string test = Properties.Settings.Default.DefaultScheduleFile;
            var path = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath;
            log.Info("path = " + path);
            path = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoaming).FilePath;
            log.Info("path = " + path);
            path = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).FilePath;
            log.Info("path = " + path);

            calendar1.SetViewRange(new DateTime(2001, 1, 1), new DateTime(2001, 1, 7));

            TimeConfig TimeConfig = new TimeConfig();
            TimeConfig.AddTimeInterval("Weekend", DayOfWeek.Saturday, new TimeSpan(5, 0, 0), new TimeSpan(10, 0, 0), new TimeSpan(3, 0, 0));
            TimeConfig.AddTimeInterval("Weekend", DayOfWeek.Sunday, new TimeSpan(5, 0, 0), new TimeSpan(10, 0, 0), new TimeSpan(3, 0, 0));
            TimeConfig.AddTimeInterval("Weekend", DayOfWeek.Sunday, new TimeSpan(20, 0, 0), new TimeSpan(22, 0, 0), new TimeSpan(1, 0, 0));
            TimeConfig.AddTimeInterval("Weekday Morning", DayOfWeek.Monday, new TimeSpan(5, 0, 0), new TimeSpan(8, 0, 0), new TimeSpan(0, 30, 0));
            TimeConfig.AddTimeInterval("Weekday Afternoon", DayOfWeek.Monday, new TimeSpan(15, 0, 0), new TimeSpan(20, 0, 0), new TimeSpan(0, 60, 0));
            TimeConfig.AddTimeInterval("Weekday Morning", DayOfWeek.Tuesday, new TimeSpan(5, 0, 0), new TimeSpan(8, 0, 0), new TimeSpan(0, 30, 0));
            TimeConfig.AddTimeInterval("Weekday Afternoon", DayOfWeek.Tuesday, new TimeSpan(15, 0, 0), new TimeSpan(20, 0, 0), new TimeSpan(0, 60, 0));
            TimeConfig.AddTimeInterval("Weekday Morning", DayOfWeek.Wednesday, new TimeSpan(5, 0, 0), new TimeSpan(8, 0, 0), new TimeSpan(0, 30, 0));
            TimeConfig.AddTimeInterval("Weekday Afternoon", DayOfWeek.Wednesday, new TimeSpan(15, 0, 0), new TimeSpan(20, 0, 0), new TimeSpan(0, 60, 0));
            TimeConfig.AddTimeInterval("Weekday Morning", DayOfWeek.Thursday, new TimeSpan(5, 0, 0), new TimeSpan(8, 0, 0), new TimeSpan(0, 30, 0));
            TimeConfig.AddTimeInterval("Weekday Afternoon", DayOfWeek.Thursday, new TimeSpan(15, 0, 0), new TimeSpan(20, 0, 0), new TimeSpan(0, 60, 0));
            TimeConfig.AddTimeInterval("Weekday Morning", DayOfWeek.Friday, new TimeSpan(5, 0, 0), new TimeSpan(8, 0, 0), new TimeSpan(0, 30, 0));
            TimeConfig.AddTimeInterval("Weekday Afternoon", DayOfWeek.Friday, new TimeSpan(15, 0, 0), new TimeSpan(20, 0, 0), new TimeSpan(0, 60, 0));
            List<TimeInterval> TimeIntervals = TimeConfig.GetTimeIntervals(DateTime.Now.DayOfWeek, DateTime.Now.TimeOfDay);
            string xml = TimeConfig.GetXMLFromObject(TimeConfig);

            StartTimeMonitor(TimeConfig);

            /*
            foreach (DayOfWeek DayOfWeek in Enum.GetValues(typeof(DayOfWeek)))
            {
                int dayOffset = (((Int32)DayOfWeek) -1 + 7) % 7;
                foreach (TimeInterval TimeInterval in TimeConfig.GetTimeIntervals(DayOfWeek, new TimeSpan(24, 0, 0)))
                {
                    
                    CalendarItem item_ = new CalendarItem(this.calendar1, new DateTime(2001, 1, 1 + dayOffset, 1, 0, 0), new DateTime(2001, 1, 1 + dayOffset, 1, 0, 0) + new TimeSpan(1, 0, 0), "Hi Oni! I love you.");
                    if (this.calendar1.ViewIntersects(item_))
                    {
                        this.calendar1.Items.Add(item_);
                    }
                }
            }
            */

            /*
            CalendarItem item = new CalendarItem(this.calendar1, new DateTime(2001, 1, 7, 13, 0, 0), new DateTime(2001, 1, 7, 13, 0, 0) + new TimeSpan(1, 0, 0), "Hi Oni! I love you.");
            m_ScheduleItems.Add(item);
            foreach (CalendarItem calendarItem in m_ScheduleItems)
            {
                if (this.calendar1.ViewIntersects(calendarItem))
                {
                    this.calendar1.Items.Add(calendarItem);
                }
            }
            */


        }

        private void StartTimeMonitor(TimeConfig TimeConfig)
        {
            TimeMonitor = new TimeMonitor(TimeConfig);
            TimeMonitorThread = new Thread(new ThreadStart(TimeMonitor.Run));
            TimeMonitorThread.Start();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                Hide();
                notifyIcon1.Visible = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //calendar1.add




        }

        private void saveScheduleMainMenuItem_Click(object sender, EventArgs e)
        {
            string saveFileName = null;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.InitialDirectory = @"C:\Temp\";
            saveFileDialog1.Title = "Save Schedule File";
            saveFileDialog1.CheckPathExists = true;
            saveFileDialog1.DefaultExt = "xml";
            saveFileDialog1.Filter = "Text files (*.xml)|*.xml|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                saveFileName = saveFileDialog1.FileName;
            }

            if (saveFileName != null)
            {
                XmlSerializer serializer = new XmlSerializer((typeof(List<CalendarItemValues>)));
                TextWriter textWriter = new StreamWriter(saveFileName);

                CalendarItemCollection Items = calendar1.Items;
                List<CalendarItemValues> CalendarItemValuesList = new List<CalendarItemValues>();
                foreach (CalendarItem CalendarItem in Items)
                {
                    System.Console.WriteLine(CalendarItem.ToString());
                    CalendarItemValuesList.Add(new CalendarItemValues(CalendarItem.StartDate, CalendarItem.EndDate, CalendarItem.Text));
                }
                serializer.Serialize(textWriter, CalendarItemValuesList);
                textWriter.Close();
            }
        }

        private void openScheduleMainMenuItem_Click(object sender, EventArgs e)
        {
            string openFileName = null;

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = @"C:\Temp\";
            openFileDialog1.Title = "Save Schedule File";
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;
            openFileDialog1.DefaultExt = "xml";
            openFileDialog1.Filter = "Text files (*.xml)|*.xml|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.ReadOnlyChecked = true;
            openFileDialog1.ShowReadOnly = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                openFileName = openFileDialog1.FileName;
            }

            if (openFileName != null)
            {
                TextReader reader = null;
                List<CalendarItemValues> CalendarItemValuesList = null;
                try
                {
                    var deserializer = new XmlSerializer(typeof(List<CalendarItemValues>));
                    reader = new StreamReader(openFileName);
                    CalendarItemValuesList = (List<CalendarItemValues>)deserializer.Deserialize(reader);
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }

                foreach (CalendarItemValues CalendarItemValues in CalendarItemValuesList)
                {
                    CalendarItem item_ = new CalendarItem(this.calendar1, CalendarItemValues.StartDate, CalendarItemValues.EndDate, CalendarItemValues.Text);
                    if (this.calendar1.ViewIntersects(item_))
                    {
                        this.calendar1.Items.Add(item_);
                    }
                }
            }
        }

        static void ReadSetting(string key)
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                string result = appSettings[key] ?? "Not Found";
                Console.WriteLine(result);
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Error reading app settings");
            }
        }

        static void AddUpdateAppSettings(string key, string value)
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                if (settings[key] == null)
                {
                    settings.Add(key, value);
                }
                else
                {
                    settings[key].Value = value;
                }
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Error writing app settings");
            }
        }

        private void stopStartLimiterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ((TimeMonitorThread == null) & (stopStartLimiterToolStripMenuItem.Text == "Start Limiter"))
            {
                TimeConfig TimeConfig = new TimeConfig();
                TimeConfig.AddTimeInterval("Weekend", DayOfWeek.Saturday, new TimeSpan(5, 0, 0), new TimeSpan(10, 0, 0), new TimeSpan(3, 0, 0));
                TimeConfig.AddTimeInterval("Weekend", DayOfWeek.Sunday, new TimeSpan(5, 0, 0), new TimeSpan(10, 0, 0), new TimeSpan(3, 0, 0));
                TimeConfig.AddTimeInterval("Weekday Morning", DayOfWeek.Wednesday, new TimeSpan(5, 0, 0), new TimeSpan(8, 0, 0), new TimeSpan(0, 30, 0));
                TimeConfig.AddTimeInterval("Weekday Morning", DayOfWeek.Wednesday, new TimeSpan(15, 0, 0), new TimeSpan(22, 0, 0), new TimeSpan(0, 0, 20));
                TimeConfig.AddTimeInterval("Weekday Morning", DayOfWeek.Thursday, new TimeSpan(0, 0, 0), new TimeSpan(22, 0, 0), new TimeSpan(0, 5, 0));
                List<TimeInterval> TimeIntervals = TimeConfig.GetTimeIntervals(DateTime.Now.DayOfWeek, DateTime.Now.TimeOfDay);
                string xml = TimeConfig.GetXMLFromObject(TimeConfig);

                StartTimeMonitor(TimeConfig);
            }
            else if ((TimeMonitorThread != null) & (stopStartLimiterToolStripMenuItem.Text == "Stop Limiter"))
            {
                TimeMonitor.Stop();
                TimeMonitorThread.Join();
            }
        }
    }

    public class CalendarItemValues
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Text  { get; set; }

        public CalendarItemValues()
        {

        }

        //[Serializable]
        public CalendarItemValues(DateTime StartDate, DateTime EndDate, string Text)
        {
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.Text = Text;
        }
    }
}
