﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace KidsTimeLimiter.Configuration
{
    public class TimeConfig
    {
        List<Day> Days = null;
        public TimeConfig()
        {
            Days = new List<Day>();
            foreach (DayOfWeek DayOfWeek in Enum.GetValues(typeof(DayOfWeek)))
            {
                Days.Add(new Day(DayOfWeek));
            }
        }


        public void AddTimeInterval(string Label, DayOfWeek DayOfWeek, TimeSpan LocalStartTime, TimeSpan LocalEndTime, TimeSpan TimeSpan)
        {
            Day Day = GetDay(DayOfWeek);
            Day.AddTimeInterval(Label, LocalStartTime, LocalEndTime, TimeSpan);
        }


        private Day GetDay(DayOfWeek DayOfWeek)
        {
            List<Day> Days = this.Days.Where(d => d.DayOfWeek == DayOfWeek).ToList();
            if (Days.Count != 1)
                new Exception("Should only have 1 day");
            Day Day = Days[0];
            return Day;
        }


        public List<TimeInterval> GetTimeIntervals(DayOfWeek DayOfWeek, TimeSpan Time)
        {
            Day Day = GetDay(DayOfWeek);
            List<TimeInterval> TimeIntervals = Day.GetTimeIntervals(Time);
            return (TimeIntervals);
        }


        public static string GetXMLFromObject(object o)
        {
            StringWriter sw = new StringWriter();
            XmlTextWriter tw = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(o.GetType());
                tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, o);
            }
            catch (Exception ex)
            {
                //Handle Exception Code
            }
            finally
            {
                sw.Close();
                if (tw != null)
                {
                    tw.Close();
                }
            }
            return sw.ToString();
        }


        public static Object ObjectToXML(string xml, Type objectType)
        {
            StringReader strReader = null;
            XmlSerializer serializer = null;
            XmlTextReader xmlReader = null;
            Object obj = null;
            try
            {
                strReader = new StringReader(xml);
                serializer = new XmlSerializer(objectType);
                xmlReader = new XmlTextReader(strReader);
                obj = serializer.Deserialize(xmlReader);
            }
            catch (Exception exp)
            {
                //Handle Exception Code
            }
            finally
            {
                if (xmlReader != null)
                {
                    xmlReader.Close();
                }
                if (strReader != null)
                {
                    strReader.Close();
                }
            }
            return obj;
        }
    }

    public class Day
    {
        public DayOfWeek DayOfWeek;
        List<TimeInterval> TimeIntervals = null;

        public Day(DayOfWeek DayOfWeek)
        {
            this.DayOfWeek = DayOfWeek;
            TimeIntervals = new List<TimeInterval>();
        }

        public void AddTimeInterval(string Label, TimeSpan LocalStartTime, TimeSpan LocalEndTime, TimeSpan TimeSpan)
        {
            TimeIntervals.Add(new TimeInterval(Label, LocalStartTime, LocalEndTime, TimeSpan));
        }

        public List<TimeInterval> GetTimeIntervals(TimeSpan Time)
        {
            List<TimeInterval> TimeIntervals;
            if (Time != null)
            {
                TimeIntervals = new List<TimeInterval>();
                foreach (TimeInterval TimeInterval in this.TimeIntervals)
                {
                    if (TimeInterval.InTimeInterval(Time))
                        TimeIntervals.Add(TimeInterval);
                }
            }
            else
            {
                TimeIntervals = this.TimeIntervals;
            }
            return TimeIntervals;
        }

    }

    public class TimeInterval
    {
        string Label;
        TimeSpan LocalStartTime;
        TimeSpan LocalEndTime;
        TimeSpan TimeAllowed;
        TimeSpan TimeRemaining;
        DateTime BaseTime = new DateTime(1900, 1, 1, 0, 0, 0);

        public TimeInterval()
        {

        }

        public TimeInterval(string Label, TimeSpan LocalStartTime, TimeSpan LocalEndTime, TimeSpan TimeSpan)
        {
            this.Label = Label;
            this.LocalStartTime = LocalStartTime;
            this.LocalEndTime = LocalEndTime;
            this.TimeAllowed = TimeSpan;
            this.TimeRemaining = TimeSpan;
        }

        public bool InTimeInterval(TimeSpan Time)
        {
            DateTime tsa = (this.BaseTime + this.LocalStartTime);
            DateTime tsb = (this.BaseTime + Time);
            DateTime tsc = (this.BaseTime + this.LocalEndTime);
            DateTime tsd = (this.BaseTime + Time);
            bool a = tsa < tsb;
            bool b = tsc > tsd;
            if ( ((this.BaseTime + this.LocalStartTime) < (this.BaseTime + Time)) & ((this.BaseTime + this.LocalEndTime) > (this.BaseTime + Time)) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public TimeSpan DecrementTimeRemaining(TimeSpan TimeIncrement)
        {
            this.TimeRemaining -= TimeIncrement;
            return(this.TimeRemaining);
        }

        public TimeSpan TimeLeft(TimeSpan Time)
        {
            return(this.LocalEndTime - Time);
        }

    }

}
