﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

using log4net;

namespace KidsTimeLimiter.Configuration
{

    class TimeMonitor
    {
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        internal static extern void LockWorkStation();



        [DllImport("user32", EntryPoint = "OpenDesktopA",
                     CharSet = CharSet.Ansi,
                     SetLastError = true,
                     ExactSpelling = true)]
        private static extern Int32 OpenDesktop(string lpszDesktop,
                                        Int32 dwFlags,
                                        bool fInherit,
                                        Int32 dwDesiredAccess);

        [DllImport("user32", CharSet = CharSet.Ansi,
                             SetLastError = true,
                             ExactSpelling = true)]
        private static extern Int32 CloseDesktop(Int32 hDesktop);

        [DllImport("user32", CharSet = CharSet.Ansi,
                             SetLastError = true,
                             ExactSpelling = true)]
        private static extern Int32 SwitchDesktop(Int32 hDesktop);

        public static bool IsWorkstationLocked()
        {
            const int DESKTOP_SWITCHDESKTOP = 256;
            int hwnd = -1;
            int rtn = -1;

            hwnd = OpenDesktop("Default", 0, false, DESKTOP_SWITCHDESKTOP);

            if (hwnd != 0)
            {
                rtn = SwitchDesktop(hwnd);
                if (rtn == 0)
                {
                    // Locked
                    CloseDesktop(hwnd);
                    return true;
                }
                else
                {
                    // Not locked
                    CloseDesktop(hwnd);
                }
            }
            else
            {
                // Error: "Could not access the desktop..."
            }
            return false;
        }


        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        volatile bool Running = false;
        volatile bool StopRunning = false;
        TimeConfig TimeConfig = null;
        TimeSpan LoopSleepTime = new TimeSpan(0, 0, 60);

        public TimeMonitor(TimeConfig TimeConfig)
        {
            this.Running = false;
            this.TimeConfig = TimeConfig;
        }

        public void Run()
        {
            this.Running = true;
            this.StopRunning = false;
            while (this.StopRunning == false)
            {
                try
                {
                    if (!IsWorkstationLocked())
                    {
                        log.Info("Workstation is not locked.  Checking for Time Intervals.");

                        bool DoLockWorkstation = false;
                        List<TimeInterval> TimeIntervals = this.TimeConfig.GetTimeIntervals(DateTime.Now.DayOfWeek, DateTime.Now.TimeOfDay);
                        log.Info(string.Format("{0} TimeInterval found", TimeIntervals.Count));


                        if (TimeIntervals.Count == 0)
                        {
                            log.Info(string.Format("No TimeInterval found - Locking workstation"));
                            ShowMessageBox("Time is up!", "Kids Time Limiter");
                            System.Threading.Thread.Sleep(10);
                            DoLockWorkstation = true;
                        }
                        else if (TimeIntervals.Count == 1)
                        {
                            TimeInterval TimeInterval = TimeIntervals[0];
                            TimeSpan TimeRemainingInLimit = TimeInterval.DecrementTimeRemaining(LoopSleepTime);
                            TimeSpan TimeRemainingInInterval = TimeInterval.TimeLeft(DateTime.Now.TimeOfDay);
                            TimeSpan TimeRemainingMin = TimeRemainingInLimit;
                            if (TimeRemainingInLimit > TimeRemainingInInterval)
                                TimeRemainingMin = TimeRemainingInInterval;
                            log.Info(string.Format("1 Time Interval found - {0} seconds remaining in time limit, {1} seconds remaining in TimeInterval", TimeRemainingInLimit.TotalSeconds, TimeRemainingInInterval.TotalSeconds));
                            if ((TimeRemainingMin.TotalSeconds > 0) & (TimeRemainingMin.TotalSeconds < 180))
                            {
                                ShowMessageBox("Warning, you have " + TimeRemainingMin.TotalSeconds.ToString() + " second remaining before this workstation will lock.", "Kids Time Limiter");
                            }

                            if (TimeRemainingInLimit.TotalSeconds < 0)
                            {
                                log.Info(string.Format("1 Time Interval found - Setting DoLockWorkstation to True"));
                                DoLockWorkstation = true;
                            }
                        }
                        else
                        {
                            throw new Exception("Expecting only 0 or 1 TimeInterval objects");
                        }


                        if (DoLockWorkstation)
                        {
                            log.Info("Locking workstation");
                            LockWorkStation();
                        }
                    }
                    else
                    {
                        log.Info("Workstation is locked.  Not doing anything.");
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                }

                System.Threading.Thread.Sleep(Convert.ToInt32(1000.0 * this.LoopSleepTime.TotalSeconds));
            }
            this.Running = false;
        }

        public void Stop()
        {
            this.StopRunning = true;
        }

        private void ShowMessageBox(string text, string caption)
        {
            Thread t = new Thread(() => MyMessageBox(text, caption));
            t.Start();
        }

        private void MyMessageBox(object text, object caption)
        {
            MessageBox.Show((string)text, (string)caption, MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1, (MessageBoxOptions)0x40000);
        }
    }
}
